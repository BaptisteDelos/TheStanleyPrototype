#include "bone.h"
#include <Engine/Renderer/Mesh/Primitive/hexahedron.h>

Bone::Bone(Vector3f v0, Vector3f v1, Eigen::Matrix4f transform, Eigen::Matrix4f refPose)
    : _endpoints(std::array<Vector3f, 2>{v0, v1}), _transform{transform}, _referencePose{refPose}, _currentTransform{refPose}
{
    _blendingMatrix = _transform.transpose() * _referencePose.inverse();
}

void Bone::draw(Shader &shader)
{
    std::vector<Vector3f> bone_vertices;
    Vector3f bone_segment = _endpoints[1] - _endpoints[0];
    Eigen::Matrix3f bone_current_transform = _currentTransform.block<3,3>(0,0).transpose();
    bone_segment = bone_current_transform * bone_segment;
    Eigen::AngleAxisf aa(-90.0f * degreeRadianRatio, bone_segment);

    // Compute upward normal to the bone segment
    Vector3f bt = bone_segment.cross(Vector3f(0.0f, 1.0f, 0.0f));
    Vector3f n = bone_segment.cross(bt);
    n.normalize();

    Vector3f v0 = bone_current_transform * _endpoints[0];
    bone_vertices.emplace_back(v0);

    // Rotate around the bone main axis to place intermediate vertices
    for (int i = 0; i < 4; ++i) {
        Vector3f v = _endpoints[0] + 0.15f * bone_segment + 0.1f * n;
        bone_vertices.emplace_back(v);
        n = aa.toRotationMatrix().transpose() * n;
        n.normalize();
    }
    Vector3f v5 = bone_current_transform * _endpoints[1];
    bone_vertices.emplace_back(v5);

    Hexahedron hexahedron(bone_vertices[0], bone_vertices[1], bone_vertices[2], bone_vertices[3], bone_vertices[4], bone_vertices[5]);

    hexahedron.draw(shader);
}

std::array<Vector3f, 2> Bone::endpoints() const
{
    return _endpoints;
}

Eigen::Matrix4f Bone::transform() const
{
    return _transform;
}

Eigen::Matrix4f Bone::blendingMatrix() const
{
    return _blendingMatrix;
}

float Bone::length() const
{
    return (_endpoints[1] - _endpoints[0]).norm();
}

void Bone::updateCurrentTransform()
{
    _currentTransform = _transform * _currentTransform;
}
