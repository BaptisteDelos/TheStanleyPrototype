#ifndef MESH_H
#define MESH_H

#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
#include <OpenMesh/Core/Mesh/Handles.hh>
#include <OpenMesh/Core/Utils/vector_traits.hh>

#include <Engine/Renderer/opengl_stuff.h>
#include <Engine/Renderer/Shader/shader.h>
#include <Engine/Renderer/Mesh/bone.h>
#include <Engine/Renderer/Mesh/skeleton.h>

template<typename T, bool = std::is_arithmetic<T>::value> struct valueType {
  typedef T value_type;
};

template<typename T> struct valueType<T,false> {
  typedef typename T::Scalar value_type;
};

template<typename T> struct vectorType {
    typedef typename T::Base vector_type;
};

template<typename T, int = T::SizeAtCompileTime>
size_t sizeAtCompileTime() { return T::SizeAtCompileTime; }

template< typename T, typename = typename std::enable_if<std::is_arithmetic<T>::value>::type >
size_t sizeAtCompileTime() { return 1; }

template <typename T>
struct my_vector_traits : OpenMesh::vector_traits<T>
{
  /// Type of the vector class
  typedef typename vectorType<T>::vector_type vector_type;

  /// Type of the scalar value
  typedef typename valueType<T>::value_type  value_type;

  /// size/dimension of the vector
  static const size_t size_ = sizeAtCompileTime<T>();

  /// size/dimension of the vector
  static size_t size() { return size_; }
};

// Define custom traits
struct MyTraits : OpenMesh::DefaultTraits
{
  // Let Point and Normal be a vector of doubles
    typedef OpenMesh::Vec3f Point;
    typedef OpenMesh::Vec3f Normal;
    typedef OpenMesh::Vec2f TexCoord2D;
    typedef OpenMesh::Vec3f Color;

  // Already defined in OpenMesh::DefaultTraits
  // HalfedgeAttributes( OpenMesh::Attributes::PrevHalfedge );

  // Uncomment next line to disable attribute PrevHalfedge
  // HalfedgeAttributes( OpenMesh::Attributes::None );
  //
  // or
  //
  // HalfedgeAttributes( 0 );
};


typedef OpenMesh::TriMesh_ArrayKernelT<MyTraits> MyMesh;


class Mesh : public MyMesh
{
public:
    Mesh();
    Mesh(std::vector<Vector3f> &vertices, std::vector<GLuint> &indices, std::vector<Vector3f> &normals, std::vector<Vector2f> &texcoords);
    ~Mesh();

    void init(std::vector<Vector3f> &vertices, std::vector<GLuint> &indices, std::vector<Vector3f> &normals, std::vector<Vector2f> &texcoords);
    void initVBO(void);
    void initProperties(void);

    /// Custom attributes getters
    Eigen::Map<Eigen::Matrix<GLuint, 3, Eigen::Dynamic>> faceIndices();
    Eigen::Map<Eigen::Matrix3Xf> positions();
    Eigen::Map<Eigen::Matrix3Xf> normals();
    Eigen::Map<Eigen::Matrix3Xf> colors();
    Eigen::Map<Eigen::Matrix2Xf> texcoords();

    void draw(Shader shader);

    void load(const std::string &filename);
    void updateVBO(void);
    void updateBoundingBox(void);

    /* Loop Subdivision */
    void upSample(void);

    /* Linear Blend Skinning */
    void initLBS(void);
    void computeLBSWeights(float jointAnimRange);
    void computeSkeleton(void);
    void sendBlendingMatricesToShader(Shader &shader);
    void linearBlendSkinning(float jointInfluenceRange); // Void because all arguments are contained in the mesh structure

    Vector3f getCenter(void);

protected:
    Eigen::AlignedBox3f _bbox;

    struct glAttribProperties {
      std::string           name;
      std::string        gl_name;
      std::size_t        ncoeffs;
      std::size_t         nbytes;
      GLenum      gl_scalar_type;
      std::size_t           size;
      const void*           data;
      gl::GLuint          vbo_id;
    };

    std::vector<glAttribProperties> _attribProperties;

    // Add a vertex attribute and store its characteristics for OpenGL to use
    template<typename T>
    void registerVertexProperty(const char * name, const char * gl_name, std::vector<T> attributes);

    std::vector<GLfloat> _glVertices;
    std::vector<GLfloat> _glNormals;
    std::vector<GLuint>  _glIndices;
    std::vector<GLfloat> _glTexcoord;
    std::vector<GLfloat> _glColors;
    std::vector<GLfloat> _glWeights;
    std::vector<GLuint> _glBoneIndices;

    GLuint _vao;
    GLuint _vbo;
    GLuint _nbo;
    GLuint _tbo;
    GLuint _cbo;
    GLuint _ebo;
    GLuint _wbo;
    GLuint _bmbo;

    /* Vertex Properties */
    OpenMesh::VPropHandleT<bool> _vertexIsNew;
    OpenMesh::VPropHandleT<MyMesh::Point> _vertexNewPosition;
    OpenMesh::VPropHandleT<Vector4f> _vertexWeights;

    /* Edge Properties */
    OpenMesh::EPropHandleT<bool> _edgeIsNew;
    OpenMesh::EPropHandleT<MyMesh::Point> _edgeNewPosition;

    /* Face Properties */
    OpenMesh::FPropHandleT<bool> _faceIsNew;

    void resetProperties(void);

    Skeleton _skeleton;    
    bool _LBSComputed = false;

    /* Internal operator */

    /* Subdivision */
    void create_face(HalfedgeHandle& heh);
    OpenMesh::VertexHandle& split_edge(EdgeHandle& eh);

    /* LBS */
    float distOnBone(Bone &bone, Vector3f &p);
    bool hasSkeleton(void);

    float step(float edge0, float edge1, float x);
    float smoothstep(float edge0, float edge1, float x);
};

#endif // MESH_H
