#include "skeleton.h"


Skeleton::Skeleton()
{

}

void Skeleton::draw(Shader &shader)
{
    for (auto& b : _bones)
        b.draw(shader);
}

std::vector<Bone> Skeleton::bones() const
{
    return _bones;
}

float Skeleton::lengthTo(int boneIndex) const
{
    float len = 0.0f;

    if (boneIndex >= _bones.size())
        return len;

    for (int i = 0; i < boneIndex; ++i)
        len += _bones[i].length();

    return len;
}

void Skeleton::updateTransforms()
{
    for (auto& b : _bones)
        b.updateCurrentTransform();
}

float Skeleton::length() const
{
    float len = 0.0f;
    for (Bone b : _bones)
        len += b.length();

    return len;
}

void Skeleton::addBone(Bone& bone)
{
    _bones.emplace_back(bone);
}
