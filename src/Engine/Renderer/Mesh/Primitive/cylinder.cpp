#include "cylinder.h"

Cylinder::Cylinder(Vector3f base, Vector3f axis, float radius, float length, int subdiv1, int subdiv2)
{
    std::vector<Vector3f> vertices;
    std::vector<Vector3f> normals;
    std::vector<Vector2f> texcoords;
    std::vector<GLuint> indices;
    Vector3f x = Vector3f(0,1,0); //orthogonal(axis);
    Vector3f y = axis.cross(x);

    for(int i = 0; i < subdiv2; i++)
    {
        float offset = float(i)/float(subdiv2 - 1);
        float offset2 = (offset - 0.5) * length;
        for(int j = 0; j < subdiv1; j++)
        {
            Vector3f pos, normal;
            Vector2f uv;

            float angle = 2.*M_PI*float(j)/float(subdiv1);
            pos = base+offset2*axis+radius*cos(angle)*x+radius*sin(angle)*y;
            normal = cos(angle)*x+sin(angle)*y;
            normal.normalize();
            uv = Vector2f(offset, float(j)/float(subdiv1 - 1));

            vertices.push_back(pos);
            normals.push_back(normal);
            texcoords.emplace_back(uv);
        }
    }

    for(unsigned int i = 0; i < subdiv2 - 1; i++)
    {
        for(unsigned int j = 0; j < subdiv1; j++)
        {
            indices.emplace_back(i * subdiv1 + j);
            indices.emplace_back(i * subdiv1 + j + subdiv1);
            indices.emplace_back(i * subdiv1 + (j + 1)%subdiv1);
            indices.emplace_back(i * subdiv1 + (j + 1)%subdiv1);
            indices.emplace_back(i * subdiv1 + j + subdiv1);
            indices.emplace_back(i * subdiv1 + (j + 1)%subdiv1 + subdiv1);
        }

    }

    initProperties();
    init(vertices, indices, normals, texcoords);
    resetProperties();

    updateVBO();
}
