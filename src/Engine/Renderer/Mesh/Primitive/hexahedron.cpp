#include "hexahedron.h"

Hexahedron::Hexahedron(Vector3f v0, Vector3f v1, Vector3f v2, Vector3f v3, Vector3f v4, Vector3f v5)
{
    std::vector<Vector3f> vertices;
    std::vector<Vector3f> normals;
    std::vector<Vector2f> texcoords;
    std::vector<GLuint> indices;

    vertices.emplace_back(v0);
    vertices.emplace_back(v1);
    vertices.emplace_back(v2);
    vertices.emplace_back(v3);
    vertices.emplace_back(v4);
    vertices.emplace_back(v5);

    Vector3f cog = (v0 + v1 + v2 + v3 + v4 + v5) / 6.f;

    normals.emplace_back((v0 - cog).normalized());
    normals.emplace_back((v1 - cog).normalized());
    normals.emplace_back((v2 - cog).normalized());
    normals.emplace_back((v3 - cog).normalized());
    normals.emplace_back((v4 - cog).normalized());
    normals.emplace_back((v5 - cog).normalized());

    indices.emplace_back(0);
    indices.emplace_back(1);
    indices.emplace_back(4);

    indices.emplace_back(0);
    indices.emplace_back(2);
    indices.emplace_back(1);

    indices.emplace_back(0);
    indices.emplace_back(3);
    indices.emplace_back(2);

    indices.emplace_back(0);
    indices.emplace_back(4);
    indices.emplace_back(3);

    indices.emplace_back(5);
    indices.emplace_back(4);
    indices.emplace_back(1);

    indices.emplace_back(5);
    indices.emplace_back(1);
    indices.emplace_back(2);

    indices.emplace_back(5);
    indices.emplace_back(2);
    indices.emplace_back(3);

    indices.emplace_back(5);
    indices.emplace_back(3);
    indices.emplace_back(4);

    init(vertices, indices, normals, texcoords);

    initProperties();
    resetProperties();

    updateVBO();
}
