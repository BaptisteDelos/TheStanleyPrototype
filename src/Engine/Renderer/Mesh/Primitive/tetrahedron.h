#ifndef TETRAHEDRON_H
#define TETRAHEDRON_H

#include <Engine/Renderer/Mesh/mesh.h>


class Tetrahedron : public Mesh
{
public:
    Tetrahedron(Vector3f v0=Vector3f(-1.0f, 0.0f, 0.0f), Vector3f v1=Vector3f(0.5f, 0.0f, -0.5f), Vector3f v2=Vector3f(0.5f, 0.0f, 0.5f), Vector3f v3=Vector3f(0.0f, 1.0f, 0.0f));
};

#endif // TETRAHEDRON_H
