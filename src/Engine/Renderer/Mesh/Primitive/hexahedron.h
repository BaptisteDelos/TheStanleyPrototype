#ifndef HEXAHEDRON_H
#define HEXAHEDRON_H

#include <Engine/Renderer/Mesh/mesh.h>


class Hexahedron : public Mesh
{
public:
    Hexahedron(
            Vector3f v0=Vector3f(-2.0f, 0.0f, 0.0f),
            Vector3f v1=Vector3f(-1.5f, -0.25f, 0.25f),
            Vector3f v2=Vector3f(-1.5f, -0.25f, -0.25f),
            Vector3f v3=Vector3f(-1.5f, 0.25f, -0.25f),
            Vector3f v4=Vector3f(-1.5f, 0.25f, 0.25f),
            Vector3f v5=Vector3f(2.0f, 0.0f, 0.0f)
            );
};

#endif // HEXAHEDRON_H
