#include "model.h"

#include <eigen3/Eigen/Geometry>
#include <ProcessHelper.h>
#include <OpenMesh/Core/IO/MeshIO.hh>

Model::Model()
{

}

Model::~Model()
{

}

void Model::draw(Shader shader)
{
    for (auto &mesh : _meshes)
        mesh.draw(shader);
}

void Model::load(const std::string &filepath)
{
//    Mesh mesh;

//    if (!OpenMesh::IO::read_mesh(mesh, filepath))
//    {
//        std::cout << "ERROR::OPENMESH:: Fail to load mesh " << filepath << std::endl;
//        exit(1);
//    }

//    mesh.setupMesh();

    Assimp::Importer import;
    const aiScene *scene = import.ReadFile(filepath, /*aiProcess_Triangulate | aiProcess_FlipUVs | */aiProcess_GenNormals | aiProcess_GenUVCoords | aiProcess_JoinIdenticalVertices);

    if(!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
    {
        std::cout << "ERROR::ASSIMP::" << import.GetErrorString() << std::endl;
        return;
    }
    _folder = filepath.substr(0, filepath.find_last_of('/'));

    processNode(scene->mRootNode, scene);
}

void Model::subdivide()
{
    for (auto &mesh : _meshes)
    {
        mesh.upSample();
        mesh.updateVBO();
    }
}

void Model::addMesh(Mesh &mesh)
{
    _meshes.emplace_back(mesh);
}

void Model::LBS()
{
    for (auto& m : _meshes)
        m.linearBlendSkinning(.5);
}

void Model::processNode(aiNode *node, const aiScene *scene)
{
    // process all the node's meshes (if any)
    for(unsigned int i = 0; i < node->mNumMeshes; ++i)
    {
        aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];
        Mesh m = processMesh(mesh, scene);
        m.updateBoundingBox();

        _meshes.push_back(m);
    }
    // then do the same for each of its children
    for(unsigned int i = 0; i < node->mNumChildren; ++i)
    {
        processNode(node->mChildren[i], scene);
    }
}

Mesh Model::processMesh(aiMesh *mesh, const aiScene *scene)
{

    std::vector<Vector3f> vertices;
    std::vector<Vector3f> normals;
    std::vector<Vector2f> uvs;
    std::vector<GLuint> indices;
    Eigen::AlignedBox3f bbox;

    for(unsigned int i = 0; i < mesh->mNumVertices; i++)
    {
        Vector3f vertex;
        vertex[0] = mesh->mVertices[i].x;
        vertex[1] = mesh->mVertices[i].y;
        vertex[2] = mesh->mVertices[i].z;
        vertices.emplace_back(vertex);

        bbox.extend(vertex);

        if (mesh->HasNormals())
        {
            Vector3f normal;
            normal[0] = mesh->mNormals[i].x;
            normal[1] = mesh->mNormals[i].y;
            normal[2] = mesh->mNormals[i].z;
            normals.emplace_back(normal);
        }

        if (mesh->HasTextureCoords(0))
        {
            Vector2f uv;
            uv[0] = mesh->mTextureCoords[i]->x;
            uv[1] = mesh->mTextureCoords[i]->y;
            uvs.emplace_back(uv);
        }
    }

    // Center at origin
    for (auto& v : vertices)
    {
        v -= bbox.center();
    }

    // process indices
    for (unsigned int i = 0; i < mesh->mNumFaces; ++i)
    {
        aiFace face = mesh->mFaces[i];
        for (unsigned int j = 0; j < face.mNumIndices; ++j)
        {
            indices.emplace_back(face.mIndices[j]);
        }
    }
    // TODO : process material


    return Mesh(vertices, indices, normals, uvs);
}

