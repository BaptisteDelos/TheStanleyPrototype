#ifndef MODEL_H
#define MODEL_H

#include <Engine/Renderer/Mesh/mesh.h>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

class Model
{
public:
    Model();
    ~Model();

    void draw(Shader shader);
    void load(const std::string &filepath);
    void subdivide(void);
    void addMesh(Mesh& mesh);
    void LBS(void)
;
private:
    std::vector<Mesh> _meshes;
    std::string _folder;

    void processNode(aiNode *node, const aiScene *scene);
    Mesh processMesh(aiMesh *mesh, const aiScene *scene);

};

#endif // MODEL_H
