#ifndef SUBDIVIDER_H
#define SUBDIVIDER_H

#include <Engine/Renderer/Mesh/mesh.h>
//#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>


class Subdivider
{
public:
    Subdivider();

    void upSample(Mesh& mesh, unsigned int iter);
};

#endif // SUBDIVIDER_H
