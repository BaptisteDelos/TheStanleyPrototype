#include "bsplinesurface.h"
#include <iostream>


BSplineSurface::BSplineSurface(std::vector< std::vector<Vector3f> >& grid, int order) : _grid{grid}, _order{order}
{
    // OpenGL buffer generation
    glGenBuffers(1, &_vbo);
    glGenBuffers(1, &_ebo);
    glGenBuffers(1, &_nbo);
    glGenVertexArrays(1, &_vao);

    // Initialize the guiding curves
    for (auto& uControlPoints : grid) {
        BSplineCurve* curve = new BSplineCurve{order, uControlPoints.size()};
        curve->setControlPoints(uControlPoints);
        _guideCurves.emplace_back(curve);
    }

    // Initialize the unique generator curve
    _generatorCurve = new BSplineCurve{order, _guideCurves.size()};
}

BSplineSurface::~BSplineSurface()
{
    for (auto& guideCurve : _guideCurves)
        delete guideCurve;

    delete _generatorCurve;
}

void BSplineSurface::draw()
{
    glBindVertexArray(_vao);
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    // Set vertex attribute pointer
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(0);
    // Set normal attribute pointer
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(1);
    glDrawElements(GL_TRIANGLES, _indices.size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
}

void BSplineSurface::updateVBO()
{
    glBindVertexArray(_vao);
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    glBufferData(GL_ARRAY_BUFFER, _vertices.size() * sizeof(GLfloat), _vertices.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, _indices.size() * sizeof(GLuint), _indices.data(), GL_STATIC_DRAW);
    // Copy our normals array in a buffer for OpenGL to use
    glBindBuffer(GL_ARRAY_BUFFER, _nbo);
    glBufferData(GL_ARRAY_BUFFER, _normals.size()*sizeof (GLfloat), _normals.data(), GL_STATIC_DRAW);
    glBindVertexArray(0);
}

void BSplineSurface::computeSurface(float ustep, float vstep)
{
    // Clear old vectors and buffers
    _vertices.clear();
    _indices.clear();

    // Get grid dimension, minimal and maximal values of u and v
    int m = _grid.size() - 1;
    int n = _grid[0].size() - 1;
    float u_min = _guideCurves[0]->getNodalVector()[_order - 1];
    float u_max = _guideCurves[0]->getNodalVector()[n + 1];
    float v_min = _generatorCurve->getNodalVector()[_order - 1];
    float v_max = _generatorCurve->getNodalVector()[n + 1];

    for (float u = u_min; u <= u_max; u += ustep) {
        for (float v = v_min; v <= v_max; v += vstep) {
            Vector3f p = computePosition(u,v);

            _vertices.emplace_back(p[0]);
            _vertices.emplace_back(p[1]);
            _vertices.emplace_back(p[2]);
        }
    }

    int nPointsU = (int)(u_max - u_min)/ustep + 1;
    int nPointsV = (int)(v_max - v_min)/vstep + 1;

    for (int i = 0; i < nPointsU - 1; ++i) {
        for (int j = 0; j < nPointsV - 1; ++j) {
            // Face's first triangle
            _indices.emplace_back(i * nPointsU + j);
            _indices.emplace_back((i+1) * nPointsU + j);
            _indices.emplace_back((i+1) * nPointsU + j + 1);
            // Face's second triangle
            _indices.emplace_back(i * nPointsU + j);
            _indices.emplace_back((i+1) * nPointsU + j + 1);
            _indices.emplace_back(i * nPointsU + j + 1);
        }
    }

    updateVBO();
}

Vector3f BSplineSurface::computePosition(float u, float v)
{
    std::vector<Vector3f> vControlPoints;
    for (auto& guideCurve : _guideCurves) {
        vControlPoints.emplace_back(guideCurve->computePosition(u));
    }
    _generatorCurve->setControlPoints(vControlPoints);

    return _generatorCurve->computePosition(v);
}

Vector3f BSplineSurface::computeNormal(Vector3f p, float u, float v, float delta_u, float delta_v)
{

}

std::vector<GLfloat> BSplineSurface::getVertices() const
{
    return _vertices;
}
