#include "bsplinecurve.h"
#include <iostream>


BSplineCurve::BSplineCurve(int order, unsigned int nbControlPoints) : _order{order} {

    // Generate geometry buffers

    glGenBuffers(1, &_vbo);
    glGenBuffers(1, &_ebo);
    glGenVertexArrays(1, &_vao);

    updateNodalVector(nbControlPoints);
}

BSplineCurve::~BSplineCurve() {

}

void BSplineCurve::draw()
{
    glBindVertexArray(_vao);
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(0);
    glDrawElements(GL_LINES, _indices.size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
}

void BSplineCurve::updateVBO()
{
    // 1. Bind Vertex Array Object
    glBindVertexArray(_vao);
    // 2. Copy our vertices array in a buffer for OpenGL to use
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    glBufferData(GL_ARRAY_BUFFER, _vertices.size()*sizeof (GLfloat), _vertices.data(), GL_STATIC_DRAW);
    // 3. Copy our index array in a element buffer for OpenGL to use
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, _indices.size()*sizeof (GLfloat), _indices.data(), GL_STATIC_DRAW);
    // 4. Unbind the VAO
    glBindVertexArray(0);
}

void BSplineCurve::computeCurve(float ustep)
{
    // Clear old vectors and buffers
    _vertices.clear();
    _indices.clear();

    int n = _controlPoints.size() - 1;

    for (float u = _nodalVector[_order - 1]; u <= _nodalVector[n + 1]; u += ustep) {

        Vector3f p = computePosition(u);

        _vertices.emplace_back(p[0]);
        _vertices.emplace_back(p[1]);
        _vertices.emplace_back(p[2]);
    }

    for (unsigned int i = 0; i < _vertices.size()/3. - 1;) {
        _indices.emplace_back(i);
        _indices.emplace_back(++i);
    }

//    std::cout << "nb points : " << _vertices.size() / 3. << std::endl;

    updateVBO();
}

void BSplineCurve::addControlPoint(Vector3f point)
{
    _controlPoints.emplace_back(point);
}

int BSplineCurve::getShift(float u)
{
    int shift = 0;
    int i = _order;

    while (u > _nodalVector[i]) {
        shift++;
        i++;
    }

    return shift;
}

Vector3f BSplineCurve::computePosition(float u)
{
    // Determine the definition interval in the nodal vector
    int shift = getShift(u);

    std::vector<Vector3f> influencePoints(_order);

    // Gather points of influence in the computation of p(u)
    for (unsigned int i = 0; i < _order; ++i)
        influencePoints[i] = _controlPoints[shift + i];

    // Initialize the number of points to be computed to the order value. At each iteration, the number of points intermediate points decreases, all remaining stored in the influence point array
    for (unsigned int iter = 0; iter < _order; ++iter) {
        for (unsigned int j = 0; j < _order - iter - 1; ++j) {
            float u_max = (float) _nodalVector[_order + shift + j];
            float u_min = (float) _nodalVector[shift + 1 + j + iter];
            influencePoints[j] *= ((float) u_max - u) / (float)(u_max - u_min);
            influencePoints[j] += influencePoints[j + 1] * (u - (float) u_min) / (float)(u_max - u_min);
        }
    }

    return influencePoints[0];
}

void BSplineCurve::updateNodalVector(unsigned int size)
{
    _nodalVector.clear();
    int n = size - 1;

//    for (unsigned int i = 0; i < _order + n + 1; ++i)
//        _nodalVector.emplace_back(i);
    for (unsigned int i = 0; i < _order; ++i)
        _nodalVector.emplace_back(0);
    for (unsigned int i = 1; i <= n - _order + 1; ++i)
        _nodalVector.emplace_back(i);
    for (unsigned int i = 0; i < _order; ++i)
        _nodalVector.emplace_back(n - _order + 2);
}

std::vector<Vector3f> BSplineCurve::getControlPoints() const
{
    return _controlPoints;
}

std::vector<GLfloat> BSplineCurve::getVertices() const
{
    return _vertices;
}

std::vector<int> BSplineCurve::getNodalVector() const
{
    return _nodalVector;
}

void BSplineCurve::setControlPoints(const std::vector<Vector3f> &controlPoints)
{
    _controlPoints = controlPoints;
    updateNodalVector(_controlPoints.size());
}
