#ifndef BSPLINECURVE_H
#define BSPLINECURVE_H

#include <eigen3/Eigen/Core>
#include <vector>
#include "Engine/Renderer/opengl_stuff.h"

using Vector3f = Eigen::Vector3f;

class BSplineCurve {
public:
    BSplineCurve(int order, unsigned int nbControlPoints);
    ~BSplineCurve();

    void draw(void);
    void updateVBO(void);

    void computeCurve(float ustep);
    Vector3f computePosition(float u);
    void updateNodalVector(unsigned int size);

    void addControlPoint(Vector3f point);
    std::vector<Vector3f> getControlPoints() const;
    void setControlPoints(const std::vector<Vector3f> &controlPoints);
    std::vector<GLfloat> getVertices() const;
    std::vector<int> getNodalVector() const;

private:
    unsigned int _order;
    std::vector<int> _nodalVector;
    std::vector<Vector3f> _controlPoints;

    std::vector<GLfloat> _vertices;
    std::vector<GLuint> _indices;

    // OpenGL object for geometry
    GLuint _vao;
    GLuint _vbo;
    GLuint _ebo;

private:
    int getShift(float u);
};

#endif // BSPLINECURVE_H
