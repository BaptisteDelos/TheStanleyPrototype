﻿#include "mainscene.h"
#include <iostream>
#include <cmath>
#include <random>

#include <Engine/Renderer/Mesh/Primitive/cylinder.h>
#include <Engine/Renderer/Mesh/Primitive/tetrahedron.h>
#include <Engine/Renderer/Mesh/Primitive/hexahedron.h>

#define deg2rad(x) float(M_PI)*(x)/180.f


MainScene::MainScene(int width, int height)
    : Scene(width, height),
      _activecamera(0),
      _camera(nullptr) {

    // Initialize shaders
    initShaders();

    // Initialize G-buffer and attached textures
//    initGBuffer();

    // Initialize SSAO and SSAO blur framebuffers
//    initSSAO();

    // Initialize FXAA post-process framebuffer
//    initScreenRendering();

    _program = _programdefault;

    _cameraselector.push_back( []()->Camera*{return new EulerCamera(glm::vec3(0.f, 0.f, 7.f));} );
    _cameraselector.push_back( []()->Camera*{return new TrackballCamera(glm::vec3(0.f, 0.f, 7.f),glm::vec3(0.f, 1.f, 0.f),glm::vec3(0.f, 0.f, 0.f));} );

    _camera.reset(_cameraselector[_activecamera]());

    _camera->setviewport(glm::vec4(0.f, 0.f, _width, _height));
    _view = _camera->viewmatrix();

    _projection = glm::perspective(_camera->zoom(), float(_width) / _height, 0.1f, 200.0f);

//    _meshmodel.load(std::string(DATA_DIR)+"/models/Cute_Darth_Vader.STL");
//    _meshmodel.load(std::string(DATA_DIR)+"/models/icosahedron.ply");
//    _meshmodel.load(std::string(DATA_DIR)+"/models/teapot.ply");
//    Mesh m = makeCylinder();
//    _meshmodel.addMesh(m);
    Cylinder cylinder;
	glCheckError();
    cylinder.initLBS();
	glCheckError();
//    Hexahedron h;
    _meshmodel.addMesh(cylinder);
    glCheckError();
}

MainScene::~MainScene()
{

}

/* ***********************************************************************************************************************
 */
Mesh MainScene::makeCylinder(Vector3f base, Vector3f axis, float radius, float length, int subdiv1, int subdiv2)
{
    std::vector<Vector3f> vertices;
    std::vector<Vector3f> normals;
    std::vector<Vector2f> texcoords;
    std::vector<GLuint> indices;
    Vector3f x = Vector3f(0,1,0); //orthogonal(axis);
    Vector3f y = axis.cross(x);

    for(int i = 0; i < subdiv2; i++)
    {
        float offset = float(i)/float(subdiv2 - 1);
        float offset2 = (offset - 0.5) * length;
        for(int j = 0; j < subdiv1; j++)
        {
            Vector3f pos, normal;
            Vector2f uv;

            float angle = 2.*M_PI*float(j)/float(subdiv1);
            pos = base+offset2*axis+radius*cos(angle)*x+radius*sin(angle)*y;
            normal = cos(angle)*x+sin(angle)*y;
            normal.normalize();
            uv = Vector2f(offset, float(j)/float(subdiv1 - 1));

            vertices.push_back(pos);
            normals.push_back(normal);
            texcoords.emplace_back(uv);
        }
    }

    for(unsigned int i = 0; i < subdiv2 - 1; i++)
    {
        for(unsigned int j = 0; j < subdiv1; j++)
        {
            indices.emplace_back(i * subdiv1 + j);
            indices.emplace_back(i * subdiv1 + j + subdiv1);
            indices.emplace_back(i * subdiv1 + (j + 1)%subdiv1);
            indices.emplace_back(i * subdiv1 + (j + 1)%subdiv1);
            indices.emplace_back(i * subdiv1 + j + subdiv1);
            indices.emplace_back(i * subdiv1 + (j + 1)%subdiv1 + subdiv1);
        }

    }

    return Mesh(vertices, indices, normals, texcoords);
}

/* ***********************************************************************************************************************
 */
void MainScene::initShaders()
{
    // Object space shaders
    _programdefault.loadFromFiles(_shaderPaths[DEFAULT]+".vert", _shaderPaths[DEFAULT]+".frag");
    _programdiffuse.loadFromFiles(_shaderPaths[DEFAULT]+".vert", _shaderPaths[BLINN]+".frag");
    _programnormal.loadFromFiles(_shaderPaths[DEFAULT]+".vert", _shaderPaths[NORMAL]+".frag");
    _programGBuffer.loadFromFiles(_shaderPaths[DEFAULT]+".vert", _shaderPaths[GBUFFER]+".frag");
    _programWeights.loadFromFiles(_shaderPaths[DEFAULT]+".vert", _shaderPaths[LBS]+".frag");

    // Screen space shaders
    _programSSAO.loadFromFiles(_shaderPaths[LIGHTPASS]+".vert", _shaderPaths[SSAO]+".frag");
    _programLighting.loadFromFiles(_shaderPaths[LIGHTPASS]+".vert", _shaderPaths[LIGHTPASS]+".frag");
    _programSSAOBlur.loadFromFiles(_shaderPaths[LIGHTPASS]+".vert", _shaderPaths[BLUR]+".frag");
    _programFXAA.loadFromFiles(_shaderPaths[LIGHTPASS]+".vert", _shaderPaths[FXAA]+".frag");

    _program = _programdefault;

    glCheckError();
}

/* ***********************************************************************************************************************
 */
void MainScene::initSSAO()
{
    // Generate random rotations
    int noise_width = 4;
    std::vector<Vector3f> ssaoNoise = genSSAONoise(noise_width);

    // Generate rotation noise texture
    glGenTextures(1, &_noiseTexture);
    glBindTexture(GL_TEXTURE_2D, _noiseTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, noise_width, noise_width, 0, GL_RGB, GL_FLOAT, ssaoNoise.data());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    // Generate SSAO framebuffer
    glGenFramebuffers(1, &_ssaoFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, _ssaoFBO);

    // Generate SSAO color texture
    glGenTextures(1, &_ssaoColorBuffer);
    glBindTexture(GL_TEXTURE_2D, _ssaoColorBuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, _width, _height, 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _ssaoColorBuffer, 0);

    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        std::cout << "ERROR::FRAMEBUFFER::SSAO:: Framebuffer is not complete" << std::endl;

    // Generate SSAO blur framebuffer
    glGenFramebuffers(1, &_ssaoBlurFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, _ssaoBlurFBO);

    // Generate SSAO blur color texture
    glGenTextures(1, &_ssaoColorBufferBlur);
    glBindTexture(GL_TEXTURE_2D, _ssaoColorBufferBlur);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, _width, _height, 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _ssaoColorBufferBlur, 0);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

/* ***********************************************************************************************************************
 */
float lerp(float a, float b, float x)
{
    return a + x * (b - a);
}

/* ***********************************************************************************************************************
 */
std::vector<Vector3f> MainScene::genSSAOKernel(int nsamples)
{
    std::uniform_real_distribution<float> distribution(0.0, 1.0);
    std::default_random_engine generator;
    std::vector<Vector3f> glSSAOKernel;

    for (unsigned int i = 0; i < nsamples; ++i) {
        Vector3f sample(
                    distribution(generator) * 2.0 - 1.0,
                    distribution(generator) * 2.0 - 1.0,
                    distribution(generator)
        );
        sample.normalize();
        sample *= distribution(generator);
        float scale = (float)i/(float)nsamples;
        scale = lerp(0.1, 1.0, scale);
        sample *= scale;

        glSSAOKernel.emplace_back(sample);
    }

    return glSSAOKernel;
}

/* ***********************************************************************************************************************
 */
std::vector<Vector3f> MainScene::genSSAONoise(int noisewidth)
{
    std::uniform_real_distribution<float> distribution(0.0, 1.0);
    std::default_random_engine generator;
    std::vector<Vector3f> noise;

    for (unsigned int i = 0; i < noisewidth*noisewidth; ++i) {
        Vector3f noiserot(
                    distribution(generator) * 2.0 - 1.0,
                    distribution(generator) * 2.0 - 1.0,
                    0.0
        );
        noise.emplace_back(noiserot);
    }

    return noise;
}

/* ***********************************************************************************************************************
 */
void MainScene::sendKernelSamplesToShader(int nsamples, Shader &shader)
{
    std::vector<Vector3f> glKernel = genSSAOKernel(nsamples);

    for (unsigned int i = 0; i < nsamples; ++i) {
        std::string sample_index = "samples["+std::to_string(i)+"]";
        shader.setVec3(sample_index, glKernel[i]);
    }
}

/* ***********************************************************************************************************************
 */
void MainScene::initGBuffer()
{
    /* ******** Screen Quad ******** */

    // Fullscreen quad VAO
    glGenVertexArrays(1, &_quadVAO);
    glBindVertexArray(_quadVAO);

    GLuint quadVBO;
    glGenBuffers(1, &quadVBO);
    glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(_quadVertices), &_quadVertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (void *)0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (void *)(3 * sizeof(GLfloat)));

    /* ******** SSAO ******** */

    // Generate G-buffer
    glGenFramebuffers(1, &_gBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, _gBuffer);

    // Generate position texture
    glGenTextures(1, &_gPosition);
    glBindTexture(GL_TEXTURE_2D, _gPosition);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, _width, _height, 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _gPosition, 0);

    // Generate normal texture
    glGenTextures(1, &_gNormal);
    glBindTexture(GL_TEXTURE_2D, _gNormal);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, _width, _height, 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, _gNormal, 0);

    // Generate albedo texture
    glGenTextures(1, &_gAlbedoSpec);
    glBindTexture(GL_TEXTURE_2D, _gAlbedoSpec);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, _width, _height, 0, GL_RGBA, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, _gAlbedoSpec, 0);

    GLenum DrawBuffers[3] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2};
    glDrawBuffers(3, DrawBuffers);

    // Generate Render Buffer Object as depth buffer
    glGenRenderbuffers(1, &_rBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _rBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, _width, _height);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, _rBuffer);

    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        std::cout << "ERROR::FRAMEBUFFER::GBUFFER:: Framebuffer is not complete" << std::endl;

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

/* ***********************************************************************************************************************
 */
void MainScene::initScreenRendering()
{
    // Generate screen framebuffer
    glGenFramebuffers(1, &_screenFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, _screenFBO);

    // Generate texture
    glGenTextures(1, &_screenTexture);
    glBindTexture(GL_TEXTURE_2D, _screenTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, _width, _height, 0, GL_RGBA, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _screenTexture, 0);

    GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
    glDrawBuffers(1, DrawBuffers);

    // Generate Render Buffer Object as depth buffer
    glGenRenderbuffers(1, &_rScreenBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _rScreenBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, _width, _height);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH24_STENCIL8, GL_RENDERBUFFER, _rScreenBuffer);

    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        std::cout << "ERROR::FRAMEBUFFER::SCREENBUFFER:: Framebuffer is not complete" << std::endl;

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

/* ***********************************************************************************************************************
 */
void MainScene::drawQuad()
{
    glBindVertexArray(_quadVAO);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);
}

/* ***********************************************************************************************************************
 */
void MainScene::resize(int width, int height){
    Scene::resize(width, height);
    _camera->setviewport(glm::vec4(0.f, 0.f, _width, _height));
}

/* ***********************************************************************************************************************
 */
void MainScene::draw() {
    // Save Qt default framebuffer
//    GLint qtBuffer;
//    glGetIntegerv(GL_FRAMEBUFFER_BINDING, &qtBuffer);

//    Scene::draw();
//    initGBuffer();
//    initSSAO();
    glCheckError();

    /***** Geometry pass *****/

//    glBindFramebuffer(GL_FRAMEBUFFER, _gBuffer);
    glEnable(GL_DEPTH_TEST);
    glViewport(0, 0, _width, _height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.24f, 0.3f, 0.3f, 1.0f);

    if (_drawfill)
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    else
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    _program.activate();

    _view = _camera->viewmatrix();
    glUniformMatrix4fv( _program.getUniformLocation("model"), 1, GL_FALSE, glm::value_ptr(_model));
    glUniformMatrix4fv( _program.getUniformLocation("view"), 1, GL_FALSE, glm::value_ptr(_view));
    glUniformMatrix4fv( _program.getUniformLocation("projection"), 1, GL_FALSE, glm::value_ptr(_projection));
	
	glCheckError();
    // Draw into G-buffer
    _meshmodel.draw(_program);

	glCheckError();

    _program.deactivate();

    /***** SSAO rendering *****/

//    glBindFramebuffer(GL_FRAMEBUFFER, _ssaoFBO);
//    glClear(GL_COLOR_BUFFER_BIT);

//    _programSSAO.activate();

//    // Send samples to shader
//    sendKernelSamplesToShader(64, _programSSAO);
//    // Bind textures
//    glActiveTexture(GL_TEXTURE0);
//    glBindTexture(GL_TEXTURE_2D, _gPosition);
//    glUniform1i(_programSSAO.getUniformLocation("gPosition"), 0);
//    glActiveTexture(GL_TEXTURE1);
//    glBindTexture(GL_TEXTURE_2D, _gNormal);
//    glUniform1i(_programSSAO.getUniformLocation("gNormal"), 1);
//    glActiveTexture(GL_TEXTURE2);
//    glBindTexture(GL_TEXTURE_2D, _noiseTexture);
//    glUniform1i(_programSSAO.getUniformLocation("texNoise"), 2);

//    glUniformMatrix4fv( _programSSAO.getUniformLocation("projection"), 1, GL_FALSE, glm::value_ptr(_projection));
//    drawQuad();

//    _programSSAO.deactivate();

    /***** SSAO Blur *****/

//    glBindFramebuffer(GL_FRAMEBUFFER, _ssaoBlurFBO);
//    glClear(GL_COLOR_BUFFER_BIT);

//    _programSSAOBlur.activate();

//    glActiveTexture(GL_TEXTURE0);
//    glBindTexture(GL_TEXTURE_2D, _ssaoColorBuffer);
//    glUniform1i(_programSSAOBlur.getUniformLocation("ssaoInput"), 0);
//    drawQuad();

//    _programSSAOBlur.deactivate();

//    /***** Lighting pass *****/

//    if (_fxaa)
//    {
//        glBindFramebuffer(GL_FRAMEBUFFER, _screenFBO);
//        glClear(GL_COLOR_BUFFER_BIT);
//    }
//    else
//    {
//        // Go back to default framebuffer
//        glBindFramebuffer(GL_FRAMEBUFFER, qtBuffer);
//        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//    }

//    _programLighting.activate();

//    // Bind textures
//    glActiveTexture(GL_TEXTURE0);
//    glBindTexture(GL_TEXTURE_2D, _gPosition);
//    glUniform1i(_programLighting.getUniformLocation("gPosition"), 0);
//    glActiveTexture(GL_TEXTURE1);
//    glBindTexture(GL_TEXTURE_2D, _gNormal);
//    glUniform1i(_programLighting.getUniformLocation("gNormal"), 1);
//    glActiveTexture(GL_TEXTURE2);
//    glBindTexture(GL_TEXTURE_2D, _gAlbedoSpec);
//    glUniform1i(_programLighting.getUniformLocation("gAlbedoSpec"), 2);
//    glActiveTexture(GL_TEXTURE3);
//    glBindTexture(GL_TEXTURE_2D, _ssaoColorBufferBlur);
//    glUniform1i(_programLighting.getUniformLocation("ssaoTexture"), 3);

//    drawQuad();

//    _programLighting.deactivate();

//    /***** FXAA pass *****/

//    if (_fxaa)
//    {
//        // Go back to default framebuffer
//        glBindFramebuffer(GL_FRAMEBUFFER, qtBuffer);
//        glClearColor(0.24f, 0.3f, 0.3f, 1.0f);
//        glDisable(GL_DEPTH_TEST);
//        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

//        _programFXAA.activate();

//        glActiveTexture(GL_TEXTURE0);
//        glBindTexture(GL_TEXTURE_2D, _screenTexture);
//        glUniform1i(_programFXAA.getUniformLocation("screenTexture"), 0);
//        Vector2f windowSize((GLfloat)_width, (GLfloat)_height);
//        glUniform2fv(_programFXAA.getUniformLocation("windowSize"), 2, windowSize.data());

//        drawQuad();

//        _programFXAA.deactivate();
    //    }
}

void MainScene::mousepressed(GLFWwindow *window, int button, int action, int mods) {
    if(action == GLFW_PRESS)
    {
        if (button == GLFW_MOUSE_BUTTON_1)
        {
            _trackingMode = TM_ROTATE_AROUND;
            _camera->processmouseclick(button, _mousex, _mousey);
        }
        else if (button == GLFW_MOUSE_BUTTON_3)
        {
            _trackingMode = TM_TRANSLATE;
        }
    }
    else if(action == GLFW_RELEASE)
    {
        _trackingMode = TM_NO_TRACK;
    }
}

/* ***********************************************************************************************************************
 */
void MainScene::mouseclick(int button, float xpos, float ypos) {
    _button = button;
    _mousex = xpos;
    _mousey = ypos;
    _camera->processmouseclick(_button, _mousex, _mousey);
}

/* ***********************************************************************************************************************
 */
void MainScene::mousemove(float xpos, float ypos) {
    _mousex = xpos;
    _mousey = ypos;
    if (_trackingMode != TM_NO_TRACK)
        _camera->processmousemovement(_button, xpos, ypos, true);
}

void MainScene::mousescroll(float xpos, float ypos)
{
    _meshmodel.LBS();
}

/* ***********************************************************************************************************************
 */
void MainScene::keyboardmove(int key, double time) {
    _camera->processkeyboard(Camera_Movement(key), time);
}

bool MainScene::keyboard(unsigned int key) {
    switch(key) {
        case 'p':
            _activecamera = (_activecamera + 1) % 2;
            _camera.reset(_cameraselector[_activecamera]());
            _camera->setviewport(glm::vec4(0.f, 0.f, _width, _height));
            return true;
        case 'c' :
            _program = _programdefault;
            return true;
        case 'n' :
            _program = _programnormal;
            return true;
        case '+' :
            _meshmodel.subdivide();
            return true;
        case '0': case '1':
        case '2': case '3':
        case '4': case '5':
        case '6': case '7':
        case '8': case '9':
            return true;
        case 'd':
            _program = _programdiffuse;
            return true;
        case 'e':
            _program = _programWeights;
            return true;
//        case 'a':
//            _fxaa = !_fxaa;
//            return true;
        case 'q':
            _meshmodel.LBS();
            return true;
        case 'w':
            toggledrawmode();
            return true;
        default:
            return false;
    }
}

/* ***********************************************************************************************************************
 */
bool MainScene::keyevent(int key, int scancode, int action, int mods) {

}
