project(StanleyPrototype)

############################################
# Configure CMake and GCC flags
cmake_minimum_required(VERSION 3.11.2) # Minimal version compatible QT5
CMAKE_POLICY(SET CMP0043 NEW) # This will silence the Cmake Warning "Policy CMP0043 is not set"

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall -pedantic -Wextra")
#set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -O3")

set(EXECUTABLE_OUTPUT_PATH ${CMAKE_SOURCE_DIR}/bin)

find_package(OpenGL REQUIRED) # define OPENGL_LIBRARIES
#find_package(Qt5Widgets REQUIRED)
#find_package(Qt5OpenGL REQUIRED)
#find_package(glbinding REQUIRED)

################################################################################
# Define project private sources and headers

FILE(GLOB_RECURSE
    SRC_FILES
    ${CMAKE_SOURCE_DIR}/src/*.cpp
)

FILE(GLOB_RECURSE
    HDR_FILES
    ${CMAKE_SOURCE_DIR}/src/*.h
)

#FILE(GLOB_RECURSE
#    UI_FILES
#    ${CMAKE_SOURCE_DIR}/src/*.ui
#)

include_directories(
   ${CMAKE_SOURCE_DIR}/src
   ${CMAKE_SOURCE_DIR}/ext
   ${CMAKE_SOURCE_DIR}/ext/glm
   ${CMAKE_SOURCE_DIR}/ext/OpenMesh
   ${CMAKE_SOURCE_DIR}/ext/OpenMesh/install/include
   ${CMAKE_SOURCE_DIR}/ext/eigen3
)

SOURCE_GROUP("Source Files" FILES ${SRC_FILES})
SOURCE_GROUP("Header Files" FILES ${HDR_FILES})
SOURCE_GROUP("Shader Files" FILES ${SHADERS})

add_definitions(-DDATA_DIR="${CMAKE_SOURCE_DIR}/data")

################################################################################
# Configure QT

#set(CMAKE_AUTOMOC ON)
#set(CMAKE_AUTOUIC ON)
#set( CMAKE_INCLUDE_CURRENT_DIR ON )

#include_directories(
#   ${Qt5Widgets_INCLUDES}
#   ${Qt5OpenGL_INCLUDES}
#)

#add_definitions(${Qt5Widgets_DEFINITIONS})
#add_definitions(${Qt5OpenGL_DEFINITIONS})

################################################################################
# Configure Assimp

set(BUILD_STATIC_LIB ON)
#set(BUILD_ASSIMP_TOOLS ON)
#set(ASSIMP_BUILD_STATIC_LIB ON)

add_subdirectory(ext/assimp)

include_directories(
   ${CMAKE_SOURCE_DIR}/ext/assimp/include
   ${CMAKE_SOURCE_DIR}/ext/assimp/code
)

################################################################################
# Configure glbinding

include_directories(
   ${CMAKE_SOURCE_DIR}/ext/glbinding/source/glbinding/include
   ${CMAKE_SOURCE_DIR}/ext/glbinding/source/glbinding-aux/include
)

add_subdirectory(${CMAKE_SOURCE_DIR}/ext/glbinding)

################################################################################
# Configure GLFW

include_directories(
   ${CMAKE_SOURCE_DIR}/ext/glfw/include
)

add_subdirectory(ext/glfw)

################################################################################
# Configure SOIL2

include(ExternalProject)

ExternalProject_Add(SOIL2_project
	SOURCE_DIR ${CMAKE_SOURCE_DIR}/ext/SOIL
	PREFIX ${CMAKE_SOURCE_DIR}/ext/SOIL
	CONFIGURE_COMMAND ""
	BUILD_COMMAND cd ${CMAKE_SOURCE_DIR}/ext/SOIL && make
	INSTALL_COMMAND ""
)

add_library(SOIL2 STATIC IMPORTED)

set_property(TARGET SOIL2 PROPERTY IMPORTED_LOCATION
			 ${CMAKE_SOURCE_DIR}/ext/SOIL/libSOIL2.so)

add_dependencies(SOIL2 SOIL2_project)

include_directories(
   ${CMAKE_SOURCE_DIR}/ext/SOIL
   ${CMAKE_SOURCE_DIR}/ext/SOIL/incs
)

################################################################################
# Build target application

add_executable(stanley
               ${SRC_FILES}
               ${HDR_FILES}
#               ${UI_FILES}
               )
#qt5_use_modules(stanley Widgets OpenGL)
set(OPENMESH_LIBS
    ${CMAKE_SOURCE_DIR}/ext/OpenMesh/install/lib/libOpenMeshCore.so.7.2
    ${CMAKE_SOURCE_DIR}/ext/OpenMesh/install/lib/libOpenMeshTools.so.7.2
)

set(EXT_LIBS assimp glfw glbinding SOIL2 ${OPENMESH_LIBS} ${OPENGL_LIBRARIES})
#set(EXT_LIBS ${QT_LIBRARIES} ${OPENGL_LIBRARIES})

target_link_libraries(stanley ${EXT_LIBS} )
