#version 410 core

in vec3 iposition;
in vec3 inormal;
in vec3 icolor;
in vec2 itexcoord;
in vec2 iweights;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec3 FragPos;
out vec3 Normal;
out vec3 Color;
out vec2 TexCoords;
out vec2 Weights;

void main()
{
    gl_Position = projection * view * model * vec4(iposition, 1.0f);

    FragPos = iposition;
    Normal = inormal;
    Color = icolor;
    TexCoords = itexcoord;
    Weights = iweights;
}
