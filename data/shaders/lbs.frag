#version 410 core

in vec3 Normal;
in vec2 Weights;

out vec4 FragColor;

void main()
{
    vec3 w0_col = vec3(1.0f, 0.0f, 0.0f);
    vec3 w1_col = vec3(0.0f, 1.0f, 0.0f);
    vec3 col = Weights[0] * w0_col + Weights[1] * w1_col;
    FragColor = vec4(col, 1.0f);
}
