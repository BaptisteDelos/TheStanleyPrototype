#version 410 core

layout (location = 0) in vec3 iposition;
layout (location = 1) in vec3 inormal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec3 FragPos;
out vec3 Normal;

void main()
{
    gl_Position = projection * view * model * vec4(iposition, 1.0f);
    Normal = inormal;
    FragPos = iposition;
}
